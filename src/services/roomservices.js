import Api from '@/services/api'

export default {
  fetchRooms () {
    return Api().get('/rooms')
  },
  postRoom (room) {
    return Api().post('/rooms', room,
      { headers: {'Content-type': 'application/json'} })
  },
  deleteRoom (id) {
    return Api().delete(`/rooms/${id}`)
  },
  fetchRoom (id) {
    return Api().get(`/rooms/${id}`)
  },
  putRoom (id, room) {
    console.log('REQUESTING ' + room._id + ' ' +
      JSON.stringify(room, null, 5))
    return Api().put(`/rooms/${id}`, room,
      { headers: {'Content-type': 'application/json'} })
  }
}
